$(document).ready(function () {
    $('#policy').popover({
        trigger:'click',
        placement:'bottom',
        content: "<span class='closingIcon' role='button'>X</span><h4>Privacy policy </h4> <p>The Client provides AirHelp with personal data under the Personal Data Protection Act or other data protection laws that may be applicable, with the explicit permission to process the personal data given and for the use thereof in the context of the Agreement.<p>",
        html: true,
        template: '<div class="popover"><div class="arrow"></div><div class="popover-content"></div></div>'
    });
    $(document).on("click", ".closingIcon", function () {
        $('#policy').trigger('click');
    });

    $('#infoIcon > i').popover({
        delay: {show: 0, hide: 500},
        trigger:'hover',
        placement:'top',
        content: "<h4>IBAN</h4> <p><strong>IBAN</strong> stands for <strong>International Bank Account Number</strong>, which is required on EU transfers, and used of many other countries worldwide.The IBAN consists of up to 34 alphanumeric characters More info <a href='https://www.getairhelp.com/pl'><span class='ibanInfo' role='button'> More info <span></a></span><p><span></span>",
        html: true,
        template: '<div class="popover"><div class="arrow"></div><div class="popover-content"></div></div>'
    });
    $('#symbolUser > a').popover({
        trigger:'focus',
        container: 'body',
        placement:'bottom',
        content: "<h4>Lorem ipsum dolor sit amet</h4> consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
        html: true,
        template: '<div class="popover"><div class="arrow"></div><div class="popover-content"></div></div>'
    });

    $('form').validator().on('submit', function (e) {
        if (e.isDefaultPrevented()) {
        }
        else {
            update(this);
            return false
        }
    }); 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
});